#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import random as rnd
import load_database11 as db
from boolionnet import boolionnet
from boolion import boolion
import torch
from itertools import product
import numpy as np
import canalizing_function_toolbox_v1_9 as can
import time

# In[231]:


rulesFolder = "update_rules_cell_collective/"
rulesFiles = sorted(os.listdir(rulesFolder))
FileNumber = 1  # 0 is the Apoptosis network
print(rulesFiles[FileNumber])
(F, I, deg, var, constants) = db.text_to_BN(rulesFolder, rulesFiles[FileNumber])
# print(len(I))
print(len(F))
print(var)
print(deg)

# In[232]:


derivativesFolder = rulesFolder[:-1] + '_derivatives/'
derivativesFiles = sorted(os.listdir(derivativesFolder))
print(derivativesFiles[FileNumber])
bn = boolionnet(inputsList=I, functionsList=F, derivativesFile=derivativesFolder + derivativesFiles[FileNumber])
# bn.currentState = torch.tensor([rnd.randint(0,1) for i in range(bn.n)])
# Initstate = bn.currentState


# In[235]:


IterFromState = 10
Iter = 10
t0 = time.time()
for i in range(Iter):
    bn.currentState = torch.tensor([rnd.randint(0, 1) for i in range(bn.n)])
    Initstate = bn.currentState
    OrigInitstate = bn.currentState
    N = len(Initstate)
    for i in range(IterFromState):
        bn.update()  # approxUpto = 'default' means no approximation
        Nextstate = bn.currentState
        Actualstate = can.update(F, I, N, Initstate)
        Initstate = Actualstate
        # print(Nextstate - Actualstate)
    Check = (Nextstate - Actualstate).numpy()
    # if Check.all() == 0:
    #     print("True")
    # else:
    if not(Check.all() == 0):
        print("False: ", OrigInitstate)
t1 = time.time()
print(t1 - t0)

# In[ ]:


# In[ ]:




