import torch
from itertools import product
import numpy as np
from boolion import boolion
import os

# example0() computes the probability that the output is 1 given the probabilities that the inputs are 1
# both 'manually' and using the boolion.computeFunctionValueProbabilistic()
def example0():
    # 'PrInputs' is a torch vector of the input probabilities.
    # PrInputs for 2-input gates
    Pr_x1, Pr_y1 = 0.1, 0.2  # sample PrInput 1
    Pr_x2, Pr_y2 = 0.4, 0.7  # sample PrInput 2
    PrInputs = torch.FloatTensor([[Pr_x1, Pr_y1],[Pr_x2, Pr_y2]]).view(2,1,-1)  # required shape: (NumSamples,1,NumInputs)
    ## PrInputs = torch.FloatTensor([[Pr_x1, Pr_y1]]).view(1,1,-1)

    # # PrInputs for 1-input gates
    # Pr_x1 = 0.1  # sample PrInput 1
    # Pr_x2 = 0.4  # sample PrInput 2
    # PrInputs = torch.FloatTensor([[Pr_x1],[Pr_x2]]).view(2,1,-1)  # required shape: (NumSamples,1,NumInputs)

    NumSamples = PrInputs.shape[0]
    NumInputs = PrInputs.shape[2]

    # # LUT entries for the OR function that go to output 1
    # NumLUTRowsToOne = 3  # the number of LUT rows that go to output 1 for the OR function
    # LUTtoOne = torch.ones(NumLUTRowsToOne,NumInputs)
    # LUTtoOne[0,0] = 0
    # LUTtoOne[1,1] = 0

    # # LUT entries for the AND function that go to output 1
    # NumLUTRowsToOne = 1  # the number of LUT rows that go to output 1 for the OR function
    # LUTtoOne = torch.ones(NumLUTRowsToOne,NumInputs)

    # LUT entries for the XOR function that go to output 1
    NumLUTRowsToOne = 2  # the number of LUT rows that go to output 1 for the OR function
    LUTtoOne = torch.ones(NumLUTRowsToOne,NumInputs)
    LUTtoOne[0,0] = 0
    LUTtoOne[1,1] = 0

    # # LUT entries for the NOT function that go to output 1
    # NumLUTRowsToOne = 1  # the number of LUT rows that go to output 1 for the OR function
    # LUTtoOne = torch.ones(NumLUTRowsToOne,NumInputs)
    # LUTtoOne[0] = 0

    # Compute the probabilistic LUT: replace 1s and 0s in the original LUT
    # with PrInputs or 1-PrInputs depending on the LUT column and whether the valus is 1 or 0 respectively
    # NOTE: there is an easier way to implement this; see boolion.computeFunctionValueProbabilistic()
    PrLUTtoOne = torch.ones(NumSamples,*LUTtoOne.shape)
    for column in range(NumInputs):
        onesidx = LUTtoOne[:, column] == 1
        zerosidx = LUTtoOne[:, column] == 0
        PrLUTtoOne[:, onesidx, column] = PrInputs[:,0,column].unsqueeze(1)
        PrLUTtoOne[:, zerosidx, column] = 1 - PrInputs[:,0,column].unsqueeze(1)

    # Compute the net probability that the output is 1, by multiplying the probabilities in each row
    # and summing them up
    PrOne = torch.prod(PrLUTtoOne,2).sum(1)  # value of Pr[f(x,y)=1]

    # Compute the same using the boolion.computeFunctionValueProbabilistic() method
    bool = boolion()  # creates the boolion object
    bool.loadLUT(LUTtoOne)
    input = PrInputs
    PrOneBoolion = bool.computeFunctionValueProbabilistic(input)

    print('Probability that the output is 1 computed \'manually\' = ', PrOne)
    print('Probability that the output is 1 computed by boolion = ', PrOneBoolion)

# example0()

# example1() computes the gradient vector (1st order partial derivatives) and the Hessian matrix (2nd order partial derivatives)
# for the two-input OR function.
# Arguments: the independent probabilities of the two inputs
# You will notice that only the gradient vector changes when the PrInputs changes.
# This can be seen as follows:
# First, the probablistic algebraic expression of the OR function is:
# f(x,y) = (1-x)*y + x*(1-y) + x*y = 1 - (1-x)*(1-y) = x + y - x*y
# Therefore, df/dx = 1-y and df/dy = 1-x; d2f/dx2 = d2f/dy2 = 0 and d2f/dxdy = d2f/dydx = -1
def example1():

    Pr_x = 0.5  # Pr(x = 1)
    Pr_y = 0.5  # Pr(y = 1)
    PrInputs = torch.FloatTensor([Pr_x, Pr_y])

    NumInputs = PrInputs.shape[0]

    params = torch.zeros(NumInputs)
    params.data = PrInputs
    params.requires_grad = True

    # LUT entries for the OR function that go to output 1
    LUTtoOne = torch.ones(3,NumInputs)
    LUTtoOne[0,0] = 0
    LUTtoOne[1,1] = 0

    zerosx, zerosy = torch.where(LUTtoOne == 0)

    # Compute the probabilistic LUT: replace 1s and 0s in the original LUT
    # with Pr_x or Pr_y (depending on the column) and 1-Pr_x or 1-Pr_y (depending on the column) respectively
    PrLUTtoOne = torch.FloatTensor(params).repeat((LUTtoOne.shape[0],1))
    PrLUTtoOne[zerosx,zerosy] = 1 - PrLUTtoOne[zerosx,zerosy]

    # Compute the net probability that the output is 1, by multiplying the probabilities in each row
    # and summing them up
    PrOne = torch.prod(PrLUTtoOne,1).sum()  # value of Pr[f(x,y)=1]

    # Compute the 1st order partial derivatives using autograd of pytorch: df/dx and df/dy
    GradientVector = torch.autograd.grad(PrOne,params,create_graph=True)[0]

    # Compute the 2nd order partial derivatives using autograd of pytorch: d2f/dx2, d2f/dy2, d2f/dxdy and d2f/dydx
    HessianMatrix = torch.zeros(NumInputs,NumInputs)
    for i in range(NumInputs):
        secondDerivatives = torch.autograd.grad(GradientVector[i],params,create_graph=False,retain_graph=True)[0]
        HessianMatrix[:,i] = secondDerivatives

    print('LUT: \n',LUTtoOne)
    print('Probablistic LUT: \n',PrLUTtoOne.data)
    print('Value of f = ',PrOne.data)  # value of Pr[f(x,y)=1]
    print('Gradient vector: \n',GradientVector.data)
    print('Hessian matrix: \n',HessianMatrix.data)

# example1()

# example2() is the simplest example that shows how to use the boolion class to compute the output of a Boolean function
# using boolion.computeFunctionValueTaylor().
# It generates a random LUT for any number of input variables and then calls the boolion methods to compute the output.
def example2():
    numInputs = 2
    l = [[0,1] for i in range(numInputs)]
    fullLUT = np.array(list(product(*l)))
    allIndices = list(range(fullLUT.shape[0]))
    r = np.random.choice(range(1,len(allIndices)))
    indicesToOne = np.random.choice(allIndices,r,replace=False)  # maps an arbitrary set of r LUT rows to 1
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])  # subset of the LUT that goes to 1
    bool = boolion()  # creates the boolion object
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    idx = np.random.choice(range(LUTtoOne.shape[0]),1)[0]  # chooses a random input that goes to 1
    input = LUTtoOne[idx,:]
    bool.computeFunctionValueTaylor(input)  # computes the output for the given input
    print('Function value of input that\'s known to map to 1:', bool.functionValue.item())
    indicesToZero = np.setdiff1d(allIndices,indicesToOne)
    LUTtoZero = torch.tensor(fullLUT[indicesToZero, :])  # subset of the LUT that goes to 0
    idx = np.random.choice(range(LUTtoZero.shape[0]),1)[0]  # chooses a random input that goes to 0
    input = LUTtoZero[idx, :]
    bool.computeFunctionValueTaylor(input)
    print('Function value of input that\'s known to map to 0:', bool.functionValue)

# example2()

# example2a() is the simplest example that shows how to use the boolion class to compute the output of a Boolean function
# using boolion.computeFunctionValueTaylorBatch() that can take a batch of inputs (NumSamples >= 1).
# It generates a random LUT for any number of input variables and then calls the boolion methods to compute the output.
def example2a():
    numInputs = 3
    l = [[0,1] for i in range(numInputs)]
    fullLUT = np.array(list(product(*l)))
    allIndices = list(range(fullLUT.shape[0]))
    r = np.random.choice(range(1,len(allIndices)))
    indicesToOne = np.random.choice(allIndices,r,replace=False)  # maps an arbitrary set of r LUT rows to 1
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])  # subset of the LUT that goes to 1
    bool = boolion()  # creates the boolion object
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    inputs = LUTtoOne  # batch input (every row is an input vector)
    bool.computeFunctionValueTaylor(inputs)  # computes the output for the given input
    print('Function value of input that\'s known to map to 1:', bool.functionValue)

# example2a()

# An example that demonstrates how to handle tautology and contradiction
def example2b():
    numInputs = 2
    l = [[0,1] for i in range(numInputs)]
    fullLUT = np.array(list(product(*l)))

    # Tautology function
    allIndices = list(range(fullLUT.shape[0]))
    indicesToOne = allIndices  # tautology
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])  # the full LUT that goes to 1
    bool = boolion()  # creates the boolion object
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    idx = np.random.choice(range(LUTtoOne.shape[0]),1)[0]  # chooses a random input that goes to 1
    input = LUTtoOne[idx,:]
    bool.computeFunctionValueTaylor(input)  # computes the output for the given input
    # since the LUT consists of entries going to one, the computed function value represents Pr[f=1]
    output = bool.functionValue.item()
    print('Function value of input that\'s known to map to 1:', output)

    indicesToZero = allIndices  # contradiction
    LUTtoZero = torch.tensor(fullLUT[indicesToZero, :])  # the full LUT that goes to 0
    idx = np.random.choice(range(LUTtoZero.shape[0]),1)[0]  # chooses a random input that goes to 0
    input = LUTtoZero[idx, :]
    bool.computeFunctionValueTaylor(input)
    # since the LUT consists of entries going to zero, the computed function value represents Pr[f=0]
    output = 1 - bool.functionValue.item()
    print('Function value of input that\'s known to map to 0:', output)

# example2b()

# example3() examines the derivatives of two NCF (nested canalyzing function) examples provided in
# Table 1 of Y. Li et al. / Theoretical Computer Science 481 (2013) 24–36
def example3():
    numInputs = 3
    l = [[0, 1] for i in range(numInputs)]
    fullLUT = np.array(list(product(*l)))
    allIndices = list(range(fullLUT.shape[0]))
    indicesToZero = np.array([6]) - 1  # NCF example 3.5 of Table 1
    indicesToOne = np.setdiff1d(allIndices, indicesToZero)
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
    print('LUT entries that go to 1: \n',LUTtoOne)
    bool = boolion()
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    print('Derivatives : \n',bool.derivatives)

    indicesToZero = np.array([1,2,4]) - 1  # NCF example 3.6 of Table 1
    indicesToOne = np.setdiff1d(allIndices, indicesToZero)
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
    print('LUT entries that go to 1: \n', LUTtoOne)
    bool = boolion()
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    print('Derivatives : \n', bool.derivatives)

# example3()

# example4() examines the derivatives of an NCF polynomial with one layer and a core polynomial, and
# a non-NCF polynomial
def example4():
    class BinaryVariable:
        def __init__(self,value=0):
            self.value = value
        def __add__(self, other):
            result = (self.value + other.value) % 2
            result = BinaryVariable(result)
            return (result)
        def __mul__(self, other):
            result = (self.value * other.value) % 2
            result = BinaryVariable(result)
            return (result)

    # David's example:
    # NCF polynomial with one layer and a core polynomial
    # g = (x1 * x2 + x1 + x2 + 1) * (x3 + x4)
    def computeBooleanFunction1(input):
        bv = [BinaryVariable(value) for value in input]
        r = ((bv[0] * bv[1]) + bv[0] + bv[1] + BinaryVariable(1)) * (bv[2] + bv[3])
        return (r.value)

    # David's example:
    # nonNCF polynomial
    # h = x1 * x2 * x3 + x2 * x3 * x4 + x2 * x3 + x1
    def computeBooleanFunction2(input):
        bv = [BinaryVariable(value) for value in input]
        r = ((bv[0] * bv[1] * bv[2]) + (bv[1] * bv[2] * bv[3]) + (bv[1] * bv[2]) + bv[0])
        return (r.value)

    numInputs = 4
    l = [[0, 1] for i in range(numInputs)]
    fullLUT = np.array(list(product(*l)))

    fullLUTOutputs = np.array(list(map(computeBooleanFunction1,fullLUT)))
    indicesToOne = np.where(fullLUTOutputs==1)[0]
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
    print('LUT entries that go to 1: \n', LUTtoOne)
    bool = boolion()
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    print('Derivatives : \n', bool.derivatives)

    fullLUTOutputs = np.array(list(map(computeBooleanFunction2, fullLUT)))
    indicesToOne = np.where(fullLUTOutputs == 1)[0]
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
    print('LUT entries that go to 1: \n', LUTtoOne)
    bool = boolion()
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    print('Derivatives : \n', bool.derivatives)

# example4()

def example5():
    def computeOR(input):
        r = input[0] + input[1] - (input[0] * input[1])
        r = int(r)
        return (r)

    def computeAND(input):
        r = (input[0] * input[1])
        r = int(r)
        return (r)

    def computeXOR(input):
        r = input[0] + input[1] - (2 * input[0] * input[1])
        r = int(r)
        return (r)

    numInputs = 2
    l = [[0, 1] for i in range(numInputs)]
    fullLUT = np.array(list(product(*l)))

    fullLUTOutputs = np.array(list(map(computeOR, fullLUT)))
    indicesToOne = np.where(fullLUTOutputs == 1)[0]
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
    print('LUT entries that go to 1: \n', LUTtoOne)
    bool = boolion()
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    print('Derivatives : \n', bool.derivatives)

    fullLUTOutputs = np.array(list(map(computeAND, fullLUT)))
    indicesToOne = np.where(fullLUTOutputs == 1)[0]
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
    print('LUT entries that go to 1: \n', LUTtoOne)
    bool = boolion()
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    print('Derivatives : \n', bool.derivatives)

    fullLUTOutputs = np.array(list(map(computeXOR, fullLUT)))
    indicesToOne = np.where(fullLUTOutputs == 1)[0]
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
    print('LUT entries that go to 1: \n', LUTtoOne)
    bool = boolion()
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()
    print('Derivatives : \n', bool.derivatives)

# example5()

# example6() is the simplest example explaining how to use boolionnet; this one explains how to define a boolionnet object for
# the Apoptosis network and compute its states up to any degree of approximation.
def example6():
    import os
    import random as rnd
    import load_database11 as db
    from boolionnet import boolionnet
    rulesFolder = "update_rules_cell_collective/"
    rulesFiles = sorted(os.listdir(rulesFolder))
    FileNumber = 0  # this is the Apoptosis network
    (F, I, deg, var, constants) = db.text_to_BN(rulesFolder,rulesFiles[FileNumber])
    derivativesFolder = rulesFolder[:-1] + '_derivatives/'
    derivativesFiles = sorted(os.listdir(derivativesFolder))
    bn = boolionnet(inputsList=I,functionsList=F,derivativesFile=derivativesFolder+derivativesFiles[FileNumber])
    currentState = torch.tensor([rnd.randint(0,1) for i in range(bn.n)])
    bn.set(currentState)
    print("Initial state = ", bn.currentState)
    bn.update(approxUpto = 1)  # approxUpto is the order of approximation (>=1 and <=n)
    # bn.update()  # approxUpto = 'default' means no approximation
    print("Next state = ", bn.currentState)

# example6()

# example7() is the simplest example explaining how to use boolionnet in the batch input (NumSamples > 1)
def example7():
    import os
    import load_database11 as db
    from boolionnet import boolionnet
    rulesFolder = "update_rules_cell_collective/"
    rulesFiles = sorted(os.listdir(rulesFolder))
    FileNumber = 0  # this is the Apoptosis network
    (F, I, deg, var, constants) = db.text_to_BN(rulesFolder,rulesFiles[FileNumber])
    derivativesFolder = rulesFolder[:-1] + '_derivatives/'
    derivativesFiles = sorted(os.listdir(derivativesFolder))
    bn = boolionnet(inputsList=I,functionsList=F,derivativesFile=derivativesFolder+derivativesFiles[FileNumber])
    NumSamples = 2
    currentState = torch.tensor(np.random.randint(2, size=(NumSamples, bn.n)))
    bn.set(currentState)
    print("Initial state = ", bn.currentState)
    bn.update(approxUpto = 1)  # approxUpto is the order of approximation (>=1 and <=n)
    # bn.update()  # approxUpto = 'default' means no approximation
    print("Next state = ", bn.currentState)

# example7()

# examples7a() explains how to check if the derivatives are correct by computing the outputs for the entire LUT
def example7a():
    import os
    import load_database11 as db
    rulesFolder = "update_rules_cell_collective/"
    rulesFiles = sorted(os.listdir(rulesFolder))
    FileNumber = 1
    (F, I, deg, var, constants) = db.text_to_BN(rulesFolder, rulesFiles[FileNumber])
    derivativesFolder = rulesFolder[:-1] + '_derivatives/'
    derivativesFiles = sorted(os.listdir(derivativesFolder))
    derivativesFile = derivativesFolder + derivativesFiles[FileNumber + 1]
    file = open(derivativesFile, 'r', encoding='UTF-8')
    contents = file.read()
    lines = contents.splitlines()
    derivativesList = []
    for line in lines:
        derivativesList.append(eval(line))
    n = len(F)
    for node in range(n):
        NumInputs = len(I[node])
        l = [[0, 1] for i in range(NumInputs)]
        fullLUT = np.array(list(product(*l)))
        indicesToOne = np.where(F[node] == 1)[0]
        bool = boolion()
        LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
        bool.loadLUT(LUTtoOne)
        bool.derivatives = derivativesList[node]
        bool.generateInputCombinations()
        # bool.decomposeFunction()  # if you use this, then comment the above two lines
        input = torch.tensor(fullLUT)
        bool.computeFunctionValueTaylor(input)
        print(bool.functionValue)

# example8() compares the outputs of boolionnet and Claus' code for the Apoptosis network. It also shows that
# boolionnet is about 20 times faster for 1000 samples run for 10 iterations.
def example8():
    import os
    import load_database11 as db
    from boolionnet import boolionnet
    import canalizing_function_toolbox_v1_9 as can
    import time
    rulesFolder = "update_rules_cell_collective/"
    rulesFiles = sorted(os.listdir(rulesFolder))
    FileNumber = 1  # this is the Apoptosis network
    print(rulesFiles[FileNumber])
    (F, I, deg, var, constants) = db.text_to_BN(rulesFolder,rulesFiles[FileNumber])
    derivativesFolder = rulesFolder[:-1] + '_derivatives/'
    derivativesFiles = sorted(os.listdir(derivativesFolder))
    NumSamples = 100
    NumIters = 500
    t0 = time.time()
    bn = boolionnet(inputsList=I, functionsList=F, derivativesFile=derivativesFolder + derivativesFiles[FileNumber+1])
    # bn = boolionnet(inputsList=I, functionsList=F, derivativesFile='')  # an empty derivative file automatically prompts boolion to compute them
    initState = torch.tensor(np.random.randint(2, size=(NumSamples, bn.n)))
    bn.set(initState)
    # bn.update(approxUpto = 1)  # approxUpto is the order of approximation (>=1 and <=n)
    bn.update(iters=NumIters)  # approxUpto = 'default' means no approximation
    t1 = time.time()
    print(t1-t0)
    t0 = time.time()
    exactFinalState = torch.zeros(NumSamples, bn.n)
    for sample in range(NumSamples):
        currentState = initState[sample]
        for iter in range(NumIters):
            nextState = can.update(F, I, bn.n, currentState)
            currentState = nextState
        exactFinalState[sample] = torch.tensor(nextState)
    t1 = time.time()
    print(t1 - t0)
    Difference = torch.pow((bn.currentState - exactFinalState),2).mean()
    print("Difference = ", Difference)

# example8()

def example9():
    from boolionnet import boolionnet
    import canalizing_function_toolbox_v1_9 as can
    NumSamples = 100
    NumIters = 25
    # Data source location
    BioInputsFuncsFolder = "inputs_funcs_bio_models/"
    BioInputsFuncsFiles = sorted(os.listdir(BioInputsFuncsFolder))
    BioDerivativesFolder = 'derivatives_bio_models/'
    # Load network
    FileNumber = 1  # 0 = Arabidopsis; 1 = B bronchiseptica
    BioModelFile = BioInputsFuncsFiles[FileNumber]
    print(BioModelFile)
    BioModel = np.load(BioInputsFuncsFolder + BioModelFile, allow_pickle=True)
    I = BioModel['arr_0']
    F = BioModel['arr_1']
    BioDerivativesFile = BioDerivativesFolder + BioModelFile.split('_Bio')[0] + '_Bio_derivatives.dat'
    bn = boolionnet(inputsList=I, functionsList=F, derivativesFile=BioDerivativesFile)
    initState = torch.tensor(np.random.randint(2, size=(NumSamples, bn.n)))
    bn.set(initState)
    bn.update(iters=NumIters,approxUpto=1)  # approxUpto is the order of approximation (>=1 and <=n); 'default' means no approximation
    exactFinalState = torch.zeros(NumSamples, bn.n)
    for sample in range(NumSamples):
        currentState = initState[sample]
        for iter in range(NumIters):
            nextState = can.update(F, I, bn.n, currentState)
            currentState = nextState
        exactFinalState[sample] = torch.tensor(nextState)
    Difference = torch.pow((bn.currentState - exactFinalState), 2).mean()
    print("Difference = ", Difference)

example9()











