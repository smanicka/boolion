import os
import load_database11 as db
from boolionnet import boolionnet
import torch
import numpy as np

# Analysis parameters
NumSamples = 5  # 1000 is the desired number
NumIters = 3  # 500 is the desired number

# Data source location
rulesFolder = "update_rules_cell_collective/"
rulesFiles = sorted(os.listdir(rulesFolder))
derivativesFolder = rulesFolder[:-1] + '_derivatives/'
derivativesFiles = sorted(os.listdir(derivativesFolder))

# Load network
FileNumber = 0  # this is the Apoptosis network
(F, I, deg, var, constants) = db.text_to_BN(rulesFolder,rulesFiles[FileNumber])
bn = boolionnet(inputsList=I, functionsList=F, derivativesFile=derivativesFolder + derivativesFiles[FileNumber])

# Analyze network
MaxOrder = max(deg)  # max order of the derivatives to be used for analyzing this network
AnalysisData = dict()
initState = torch.tensor(np.random.randint(2, size=(NumSamples,bn.n)))  # shape = (NumSamples,NumNodes)
# Compute approximated final state
bn.set(initState)
bn.update(iters=NumIters,approxUpto=MaxOrder,saveIntermediateOrders=True)
AnalysisData['initial'] = initState  # shape = (NumSamples,NumNodes)
AnalysisData['final'] = bn.currentStateAllOrders  # shape = (NumSamples,MaxOrder,NumNodes)

# Save analysis data file
filename = './Data/' + rulesFiles[FileNumber].split('.txt')[0] + '_AnalysisData.dat'
torch.save(AnalysisData,filename)

## To read the analysis file, run the following:
# filename = './Data/' + rulesFiles[FileNumber].split('.txt')[0] + '_AnalysisData.dat'
# AnalysisData = torch.load(filename)