#!/usr/bin/env python
# coding: utf-8

# In[112]:


import numpy as np
import load_database11 as db
import torch
from boolion import boolion
from itertools import product
import os
import ast
from random import shuffle
from boolionnet import boolionnet
import networkx as nx

# In[124]:


# Load the network from given location
rulesFolder = "update_rules_cell_collective/"
rulesFiles = sorted(os.listdir(rulesFolder))
derivativesFolder = rulesFolder[:-1] + '_derivatives/'
derivativesFiles = sorted(os.listdir(derivativesFolder))
FileNumber = 0  # this is the Apoptosis network
(F, I, deg, var, constants) = db.text_to_BN(rulesFolder, rulesFiles[FileNumber])

# In[32]:


# We want to keep the in-degree and reorder the values for each function array in F.  We want to change the inputs for each function in array I.
print("Degree List: ", deg)
print("Function List: ", F)
print("Input: ", I)

# In[62]:


# Rearrange the entries for each function in F
randNetwork = F

for item in randNetwork:
    shuffle(item)
print(randNetwork)

# In[71]:


# Change the variables referenced in I
randInput = []
maxVal = len(I) - 1
rng = np.random.default_rng()
for item in I:
    item = rng.choice(maxVal, size=len(item), replace=False)  # replace variables in each function without repetition
    item = item.astype('int32')  # change dtype from int64
    randInput.append(item)

print(randInput)


# In[201]:


# Make the edgelist from I
def I_to_edgelist(I):
    regulators = []  # the elements in the array associated with each function in I
    targets = []  # the position of the array in I
    index = 0
    for array in I:
        n_array = len(array)
        for k in range(n_array):
            regulators.append(array[k])  # append the elements in the array
            targets.append(index)  # repeat the position for each element in the array
        index += 1
    edgelist = list(map(lambda x, y: (x, y), regulators, targets))  # makes the ordered pairs
    return edgelist


# In[204]:


# check that the new network is connected
edgelist = I_to_edgelist(randInput)
print(edgelist)
G = nx.Graph(edgelist)
print(nx.is_connected(G))

# In[221]:


# save derivatives to file
Filename = rulesFiles[FileNumber].split('.txt')[0] + '_Random.txt'
File = open(Filename, 'w')
derivativesList = []
for i in range(len(randNetwork)):
    numInputs = int(np.log2(len(randNetwork[i])))  # this is the number of inputs for each output F[i]
    myarray = [[0, 1] for i in range(numInputs)]
    fullLUT = np.array(list(product(*myarray)))  # get the truthtable input
    allIndices = list(range(fullLUT.shape[0]))
    outputArray = randNetwork[i]
    indicesToOne = []
    for j, k in enumerate(outputArray):  # get the indices that go to one
        if k == 1:
            indicesToOne.append(j)
    LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
    bool = boolion()  # creates the boolion object
    bool.loadLUT(LUTtoOne)
    bool.decomposeFunction()  # computes the output for the given input
    derivatives = bool.derivatives
    derivativesList.append(derivatives)

for item in derivativesList:
    File.writelines(str(item) + '\n')
File.close()

# In[ ]:


bnOriginal = boolionnet(inputsList=I, functionsList=F, derivativesFile=derivativesFolder + derivativesFiles[FileNumber])
bnRandom = boolionnet(inputsList=randInput, functionsList=randNetwork, derivativesFile=Filename)

# In[ ]:




