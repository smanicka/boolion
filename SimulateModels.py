# Simulate both the bio models and the associated random ensembles, and store the simulation in a single file
# for each Bio model

import os
from boolionnet import boolionnet
import torch
import numpy as np

# Analysis parameters
NumSamples = 10  # 1000 is the desired number
NumIters = 500  # 500 is the desired number
NumRandomModels = 10  # 100 is the desired number

# Data source location
BioInputsFuncsFolder = "inputs_funcs_bio_models/"
BioInputsFuncsFiles = sorted(os.listdir(BioInputsFuncsFolder))
BioDerivativesFolder = 'derivatives_bio_models/'
RandomInputsFuncsFolder = "inputs_funcs_random_models/"
RandomDerivativesFolder = 'derivatives_random_models/'

for FileNumber in range(len(BioInputsFuncsFiles)):
    # Load network
    BioModelFile = BioInputsFuncsFiles[FileNumber]
    BioModel = np.load(BioInputsFuncsFolder + BioModelFile,allow_pickle=True)
    I = BioModel['arr_0']
    F = BioModel['arr_1']
    BioDerivativesFile = BioDerivativesFolder + BioModelFile.split('_Bio')[0] + '_Bio_derivatives.dat'
    bn = boolionnet(inputsList=I, functionsList=F, derivativesFile=BioDerivativesFile)
    NumNodes = bn.n

    # Analyze network
    indegrees = list(map(lambda l: len(l), I))
    MaxOrder = max(indegrees)  # max order of the derivatives to be used for analyzing this network
    initState = torch.tensor(np.random.randint(2, size=(NumSamples,NumNodes)))  # shape = (NumSamples,NumNodes)
    SimulationData = dict()

    # Simulate Bio model
    bn.set(initState)  # same initial state for the Bio and all the Random models
    bn.update(iters=NumIters,approxUpto=MaxOrder,saveIntermediateOrders=True)
    SimulationData['Bio'] = dict()
    SimulationData['Bio']['initial'] = initState  # shape = (NumSamples,NumNodes)
    SimulationData['Bio']['final'] = bn.currentStateAllOrders  # shape = (NumSamples,MaxOrder,NumNodes)

    # Simulate Random models associated with the above Bio model
    SimulationData['Random'] = dict()
    SimulationData['Random']['initial'] = torch.zeros(NumRandomModels,NumSamples,NumNodes)  # shape = (NumRandomModels,NumSamples,NumNodes)
    SimulationData['Random']['final'] = torch.zeros(NumRandomModels,NumSamples,MaxOrder,NumNodes)  # shape = (NumRandomModels,NumSamples,MaxOrder,NumNodes)
    for RandNetworkSample in range(NumRandomModels):
        RandomModelFile = RandomInputsFuncsFolder + BioModelFile.split('_Bio')[0] + '_Random_inputs_funcs' + str(RandNetworkSample) + '.npz'
        RandomModel = np.load(RandomModelFile, allow_pickle=True)
        I = RandomModel['arr_0']
        F = RandomModel['arr_1']
        RandomDerivativesFile = RandomDerivativesFolder + BioModelFile.split('_Bio')[0] + '_Random_derivatives' + str(RandNetworkSample) + '.dat'
        bn = boolionnet(inputsList=I, functionsList=F, derivativesFile=RandomDerivativesFile)
        bn.set(initState)  # same initial state for the Bio and all the Random models
        bn.update(iters=NumIters, approxUpto=MaxOrder, saveIntermediateOrders=True)
        SimulationData['Random']['initial'][RandNetworkSample] = initState  # shape = (NumSamples,NumNodes)
        SimulationData['Random']['final'][RandNetworkSample] = bn.currentStateAllOrders  # shape = (NumSamples,MaxOrder,NumNodes)

    # Save simulation data file
    SimulationFile = './Data/' + BioModelFile.split('_Bio')[0] + '_SimulationData.dat'
    torch.save(SimulationData,SimulationFile)

## To read the simulation file, run the following:
# filename = './Data/' + BioInputsFuncsFiles[FileNumber].split('_Bio')[0] + '_SimulationData.dat'
# SimulationData = torch.load(filename)