# Generate random ensembles for every Bio model and generate the _inputs_funcs_ files for both

import os
import load_database11 as db
import numpy as np
import torch
from itertools import product
from boolion import boolion

NumRandomModels = 100  # 100 is the desired number
preserveBias = True
rulesFolder = "rules_bio_models/"
rulesFiles = sorted(os.listdir(rulesFolder))

for FileNumber in range(len(rulesFiles)):
    print(FileNumber, rulesFiles[FileNumber])
    (F, I, deg, var, constants) = db.text_to_BN(rulesFolder, rulesFiles[FileNumber])
    bioInputsFuncsFolder = 'inputs_funcs_bio_models/'
    bioInputsFuncsFile = rulesFiles[FileNumber].split('.txt')[0] + '_Bio_inputs_funcs.npz'
    np.savez(bioInputsFuncsFolder + bioInputsFuncsFile, I, F)  # saves the inputs and the new rules
    derivativesList = []
    for i in range(len(F)):
        numInputs = int(np.log2(len(F[i])))  # this is the number of inputs for each output F[i]
        myarray = [[0, 1] for i in range(numInputs)]
        fullLUT = np.array(list(product(*myarray)))  # get the truthtable input
        allIndices = list(range(fullLUT.shape[0]))
        outputArray = F[i]
        indicesToOne = np.where(outputArray == 1)[0]
        LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
        bool = boolion()  # creates the boolion object
        bool.loadLUT(LUTtoOne)
        bool.decomposeFunction()  # computes the output for the given input
        derivatives = bool.derivatives
        derivativesList.append(derivatives)
    bioderivativesFolder = 'derivatives_bio_models/'
    biodderivativesFile = rulesFiles[FileNumber].split('.txt')[0]+ '_Bio_derivatives.dat' #name of file
    torch.save(derivativesList,bioderivativesFolder + biodderivativesFile)
    for run in range(NumRandomModels):
        # generate new rules
        randRules = []  # new rules list
        for i in range(len(F)):
            is_all_zeros = True
            is_all_ones = True
            n = len(F[i])
            if preserveBias:
                numones = sum(F[i])
                oneIndices = np.random.choice(n,numones,replace=False)
                newRule = np.zeros(n,dtype=np.int)
                newRule[oneIndices] = 1
            else:
                while (is_all_zeros == True) or (is_all_ones == True):  # don't allow all ones or all zeros in new rules
                    newRule = np.random.choice(2,n)
                    is_all_zeros = np.all((newRule==0))
                    is_all_ones = np.all((newRule==1))
            randRules.append(newRule)
        #calculate new rule derivatives
        randderivativesFolder = 'derivatives_random_models/'
        randderivativesFile = rulesFiles[FileNumber].split('.txt')[0]+ '_Random_derivatives' + str(run)+'.dat' #name of file
        derivativesList = []
        for i in range(len(randRules)):
            numInputs = int(np.log2(len(randRules[i])))  # this is the number of inputs for each output F[i]
            myarray = [[0, 1] for i in range(numInputs)]
            fullLUT = np.array(list(product(*myarray)))  # get the truthtable input
            allIndices = list(range(fullLUT.shape[0]))
            outputArray = randRules[i]
            indicesToOne = np.where(outputArray == 1)[0]
            LUTtoOne = torch.tensor(fullLUT[indicesToOne, :])
            bool = boolion()  # creates the boolion object
            bool.loadLUT(LUTtoOne)
            bool.decomposeFunction()  # computes the output for the given input
            derivatives = bool.derivatives
            derivativesList.append(derivatives)
        torch.save(derivativesList,randderivativesFolder + randderivativesFile)
        randInputsFuncsFolder = 'inputs_funcs_random_models/'
        randInputsFuncsFile = rulesFiles[FileNumber].split('.txt')[0] + '_Random_inputs_funcs' + str(run) + '.npz'
        np.savez(randInputsFuncsFolder + randInputsFuncsFile, I, randRules)  # saves the inputs and the new rules