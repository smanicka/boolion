# Compute approximation errors for both the Bio models and the associated random ensembles, and
# store the error data in a single for each Bio model

import torch
import os

# Data source location
BioInputsFuncsFolder = "inputs_funcs_bio_models/"
BioInputsFuncsFiles = sorted(os.listdir(BioInputsFuncsFolder))

for FileNumber in range(len(BioInputsFuncsFiles)):
    SimulationFile = './Data/' + BioInputsFuncsFiles[FileNumber].split('_Bio')[0] + '_SimulationData.dat'
    SimulationData = torch.load(SimulationFile)

    MaxOrder = SimulationData['Bio']['final'].shape[1]

    BioFinalExact = SimulationData['Bio']['final'][:,-1,:]  # shape = (NumSamples,MaxOrder,NumNodes)
    RandomFinalExact = SimulationData['Random']['final'][:,:,-1,:]  # shape = (NumRandomModels,NumSamples,MaxOrder,NumNodes)

    ErrorData = dict()
    ErrorData['Bio'] = dict()
    ErrorData['Random'] = dict()
    for order in range(MaxOrder):
        BioFinalApprox = SimulationData['Bio']['final'][:,order,:]  # 0 = linear; 1 = quadratic, etc.
        BioApproxError = torch.pow(BioFinalExact - BioFinalApprox, 2).mean()

        RandomFinalApprox = SimulationData['Random']['final'][:,:,order,:]  # 0 = linear; 1 = quadratic, etc.
        RandomApproxError = torch.pow(RandomFinalExact - RandomFinalApprox, 2).mean()

        ErrorData['Bio'][order] = BioApproxError.item()
        ErrorData['Random'][order] = RandomApproxError.item()

    AnalysisFile = './Data/' + BioInputsFuncsFiles[FileNumber].split('_Bio')[0] + '_AnalysisData.dat'
    torch.save(ErrorData,AnalysisFile)



